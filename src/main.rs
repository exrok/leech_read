use rouille::{Request as Req,Response as Res, router};
use leech_read::TorrentInfo;
use std::net::{ ToSocketAddrs };

fn hexal(a: u8) -> u8 {
    a - 48 - (a > 57) as u8 * 39
}

fn serialize_info_hash(hash: &str ) -> [i8;20]{
    let hash = hash.as_bytes();
    let mut buf:[i8;20] = [0;20];
    for i in 0..20 {
        buf[i] = (((hexal(hash[i*2]) * 16) + hexal(hash[i*2 +1])) as i8).to_be();
    }
    buf
}

use leech_read::udp_scraper::start_udp_listener;
use leech_read::udp_scraper::UdpRequestSender;
use std::sync::mpsc::channel;
use std::time::Duration;
fn fetch_torrent_info(udp_tx :UdpRequestSender, hash: String) -> Res {
    let (tx,rx) = channel();
    leech_read::http_scraper::http_tracker(tx.clone(), &hash);
    let bin_hash = serialize_info_hash(&hash);
    let id = udp_tx.send(&tx, bin_hash);

    drop(tx);
    let mut torrent_info = TorrentInfo::new(0,0);
    while let Ok(ti) = rx.recv_timeout(Duration::from_millis(1000)) {
        torrent_info.merge(ti);
    }
    udp_tx.close(id);
    Res::text(format!("{{\"hash\":\"{}\", \"leechers\":\"{}\",\"seeders\":\"{}\"}}\n",
                      hash,
                      torrent_info.leechers,
                      torrent_info.seeders))
}

use env_logger::{Builder,Env};
fn main() {
    Builder::from_env(Env::default().default_filter_or("warn")).init();

    let udp_tx = start_udp_listener("0.0.0.0:25565".to_socket_addrs().unwrap().next().unwrap());
    rouille::start_server("localhost:8000", move |request| {
        let udp_tx = udp_tx.clone();
        router!(request,
                (GET) (/hash/{h: String}) => {fetch_torrent_info(udp_tx,h)},
                _ => Res::empty_404()
        )
    });
}
