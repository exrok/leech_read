pub mod udp_scraper;
pub mod http_scraper;

#[derive(Debug)]
pub struct TorrentInfo{
    pub seeders:i32,
    pub leechers:i32,
}
impl TorrentInfo {
    pub fn new(seeders: i32, leechers:i32) -> TorrentInfo {
        TorrentInfo {
            seeders,
            leechers
        }
    }
    pub fn merge(&mut self, other: TorrentInfo) {
        self.seeders = std::cmp::max(self.seeders, other.seeders);
        self.leechers = std::cmp::max(self.leechers, other.leechers);
    }
}
