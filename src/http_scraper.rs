use futures::{stream, Future, Stream};
use reqwest::r#async::{Client, ClientBuilder};
use hashbrown::HashMap;
use if_chain::if_chain;
use std::sync::mpsc::Sender;
use bendy::decoder::{Decoder, Object};
use crate::TorrentInfo;

fn hexal(a: u8) -> u8 {
    a - 48 - (a > 57) as u8 * 39
}

use percent_encoding::percent_encode_byte;
fn percent_encode_info_hash(hash: &str) -> String {
    let hash = hash.as_bytes();
    let mut buf:[u8;20] = [0;20];
    for i in 0..20 {
        buf[i] = (((hexal(hash[i*2]) * 16) + hexal(hash[i*2 +1])) as u8);
    }
    buf.iter().cloned().map(percent_encode_byte).collect()
}

const HTTP_TRACKERS: &[&str] = &[
    "http://explodie.org:6969/scrape",
    "http://tracker1.itzmx.com:8080/scrape",
];

use std::thread;

fn parse_bencode(bytes:&[u8]) -> Result<TorrentInfo,bendy::Error> {
    let mut decoder = Decoder::new(&bytes);
    let object = decoder.next_object()?;
    if_chain! {
        if let Some(Object::Dict(mut dec)) = object;
        if let Some((b"files",Object::Dict(mut dec))) = dec.next_pair()?;
        if let Some((_, Object::Dict(mut dec))) = dec.next_pair()?;
        then {
            let mut seeders: Option<i32> = None;
            let mut leechers: Option<i32> = None;
            while let Some((key, value)) = dec.next_pair()? {
                match (key, value) {
                    (b"complete", Object::Integer(raw)) => {
                        seeders = raw.parse::<i32>().ok();
                    },
                    (b"incomplete", Object::Integer(raw)) => {
                        leechers = raw.parse::<i32>().ok();
                    },
                    _ => continue
                };
                if let (Some(s), Some(l)) = (seeders, leechers) {
                    return Ok(TorrentInfo::new(s,l));
                }
            }
        }
    };

    Err(bendy::Error::UnexpectedEof)
}

pub fn http_tracker(tx: Sender<TorrentInfo> , hash: &str) {
    let hash_enc = percent_encode_info_hash(hash);
    thread::spawn(move ||{
        let client = ClientBuilder::new().timeout(std::time::Duration::from_secs(1))
                                         .build().expect("failed to build client");
        let bodies = stream::iter_ok(HTTP_TRACKERS)
            .map(move |url| {
                client.get(&format!("{}?info_hash={}",url, hash_enc))
                      .send()
                      .and_then(|res| res.into_body().concat2().from_err())
            })
            .buffer_unordered(2);

        let work = bodies
            .for_each(move |b| {
                if let Ok(torrent_info) = parse_bencode(&b) {
                    tx.clone().send(torrent_info).ok();
                }
                Ok(())
            })
            .map_err(|e| println!("Error while processing: {}", e));

        tokio::run(work);
    });
}
