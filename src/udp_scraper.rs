use crate::TorrentInfo;
use std::io::Cursor;
use byteorder::{BigEndian, ReadBytesExt, ByteOrder};
use std::net::{ UdpSocket, Ipv4Addr, ToSocketAddrs, SocketAddr };
use std::sync::mpsc::{sync_channel, SyncSender,Sender, Receiver};
use std::thread;

use log::{info, trace, warn};
#[repr(packed)]
struct Request {
    connection_id:i64,
    action:i32,
    transaction_id:i32,
    info_hash:[i8;20],
}

#[repr(packed)]
struct Connect{
    connection_id:i64,
    action:i32,
    transaction_id:i32,
}
const UDP_TRACKERS: &[&str] = &[
    "tracker.coppersurfer.tk:6969",
    "9.rarbg.com:2710",
    "exodus.desync.com:6969",
    "tracker.vanitycore.co:6969",
];

impl Connect {
    fn new(id: i32) -> Connect {
        Connect {
            connection_id:0x41727101980_i64.to_be(),
            action:0,
            transaction_id:convert_transaction_id(id).to_be(),
        }
    }
}

impl Request {
    fn new(info_hash:[i8;20]) -> Request {
        Request {
            connection_id: 0,
            action:2_i32.to_be(),
            transaction_id:0,
            info_hash,
        }
    }
}

unsafe fn any_as_u8_slice<T: Sized>(p: &T) -> &[u8] {
    ::std::slice::from_raw_parts(
        (p as *const T) as *const u8,
        ::std::mem::size_of::<T>(),
    )
}

const SCRAPE_SUCCESS_RETURN_CODE:i32 = 2;
use std::io;

pub enum Packet {
    ConnectResponse(SocketAddr, i32, i64),
    ScrapeResponse(SocketAddr, i32, TorrentInfo),
    ScrapeRequest([i8;20], i32, Sender<TorrentInfo>),
    Error(SocketAddr, i32),
    Reload,
    Close(i32),
}

#[derive(Clone)]
pub struct UdpRequestSender(pub SyncSender<Packet>);
use core::sync::atomic::{AtomicUsize, Ordering};
static REQUEST_COUNTER: AtomicUsize = AtomicUsize::new(0);

impl UdpRequestSender {
    pub fn send(&self, tx: &Sender<TorrentInfo>, hash: [i8;20]) -> i32 {
        let id = (REQUEST_COUNTER.fetch_add(1, Ordering::SeqCst) % 0xffff_ffff) as i32;
        self.0.send(Packet::ScrapeRequest( hash, id, tx.clone())).unwrap();
        id
    }
    pub fn reload(&self) {
        self.0.send(Packet::Reload).unwrap();
    }
    pub fn close(&self, id: i32){
        self.0.send(Packet::Close(id)).unwrap();
    }
}


fn udp_listener(listen_addr: SocketAddr, tx: SyncSender<Packet>) -> std::io::Result<UdpSocket> {
    let socket = UdpSocket::bind(listen_addr)?;
    let thread_socket = socket.try_clone()?;
    thread::spawn(
        move || {
        let mut buf = [0;40];
        while let Ok((len, recv_addr)) = thread_socket.recv_from(&mut buf) {
            if len < 16  {
                eprintln!("Warning: UDP listener recieved smaller than expected packet");
            }
            match BigEndian::read_i32(&buf[0..4]) {
                0 => tx.send(Packet::ConnectResponse(
                    recv_addr,
                    convert_transaction_id(BigEndian::read_i32(&buf[4..8])),
                    BigEndian::read_i64(&buf[8..16]))
                ).unwrap(),
                2 => tx.send(Packet::ScrapeResponse(
                    recv_addr,
                    convert_transaction_id(BigEndian::read_i32(&buf[4..8])),
                    TorrentInfo::new(BigEndian::read_i32(&buf[8..12]),
                                     BigEndian::read_i32(&buf[16..20])))
                ).unwrap(),
                3 => {
                    tx.send(Packet::Error(
                        recv_addr,
                        convert_transaction_id(BigEndian::read_i32(&buf[4..8]))
                    )).unwrap();
                    let err_msg:String = buf[8..len].iter().map(|c| char::from(c.to_le()) ).collect();
                    warn!("UDP_ERROR_RESPONSE:{}",err_msg );
                },
                _ => (),
            };
        }});
    Ok(socket)
}
use hashbrown::HashMap;

fn convert_transaction_id(id :i32) -> i32 {
   ( (id as u32 ) ^ 0xdead_beaf) as i32
}


fn udp_packet_processor(socket: UdpSocket,rx: Receiver<Packet>) {
    let mut req_table: HashMap<i32, (Sender<TorrentInfo>, usize)> = HashMap::default();
    let mut trackers:Vec<(SocketAddr, i64)> = vec![];
    let mut tracker_table:HashMap<SocketAddr, (usize, &'static str)> =  HashMap::default();
    {//Initilize Trackers
        info!("Intializing UDP Trackers Started");
        let connection_packet = Connect::new(0);
        let bytes: &[u8] = unsafe { any_as_u8_slice(&connection_packet) };

        for uri in UDP_TRACKERS {
            if let Ok(mut res) = uri.to_socket_addrs() {
                if let Some(res) = res.next() {
                    match socket.send_to(&bytes, res) {
                        Err(err) => {
                            eprintln!("Error Sending Connection Packet to: [{:?}] {:?}", uri,err);
                        },
                        Ok(_) => {
                            tracker_table.insert(res, (trackers.len(), uri));
                            trackers.push((res, 0));
                        }
                    }
                }
            }
        }
        info!("Intializing UDP Trackers Complete");
    }
    for packet in rx.iter() {
        match packet {
            Packet::ConnectResponse(addr, _id, connection_id) => {
                info!("SCRAPE_CONNECTION_RESPONSE[TCP][{:?},{:?}]",addr, connection_id);
                if let Some((index,uri)) = tracker_table.get(&addr) {
                    info!("SCRAPE_UPDATED[TCP][{:?}]",uri);
                    trackers[*index].1 = connection_id.to_be();
                }
            },
            Packet::ScrapeResponse(_addr, id, torrent_info) => {
                if let Some((ref tx, ref mut req_waiting)) = req_table.get_mut(&id) {
                    info!("SCRAPE_RESPONSE[TCP][{:?}:L={},S={}]",_addr,
                          torrent_info.leechers,
                          torrent_info.seeders);
                    tx.send(torrent_info).is_ok();
                    *req_waiting -= 1;
                    if *req_waiting == 0 {
                       req_table.remove(&id);
                    }
                }
            },
            Packet::ScrapeRequest(hash, id, tx) => {
                info!("SCRAPE_RESQUEST[TCP][HASH:{:?}:id={}]",hash,id);
                req_table.insert(id, (tx, trackers.len()));
                let mut request_packet = Request::new(hash);
                request_packet.transaction_id = convert_transaction_id(id).to_be();
                for (addr, id) in &trackers {
                    request_packet.connection_id = *id;
                    let bytes: &[u8] = unsafe { any_as_u8_slice(&request_packet) };
                    if socket.send_to(&bytes, addr).is_err(){
                        eprintln!("Error Sending Scrape Packet to: {:?}", addr);
                    }
                }
            },
            Packet::Reload => {
                let connection_packet = Connect::new(0);
                let bytes: &[u8] = unsafe { any_as_u8_slice(&connection_packet) };
                for (addr, _) in &trackers {
                    if socket.send_to(&bytes, addr).is_err(){
                        eprintln!("Error Sending Connection while Reloading: {:?}", addr);
                    }
                }
            },
            Packet::Error(addr, id) => {
                info!("SCRAPE_ERROR[TCP][{:?}]",addr);
                //Try to reastablish connection
                let connection_packet = Connect::new(id);
                let bytes: &[u8] = unsafe { any_as_u8_slice(&connection_packet) };
                if socket.send_to(&bytes, addr).is_err(){
                    eprintln!("Error Sending Connection Packet to: {:?}", addr);
                }
            },
            Packet::Close(id) => {
                req_table.remove(&id);
            }
        }
    }

    info!("udp_packet_processor cloesed");
}

pub fn start_udp_listener(listen_addr: SocketAddr) -> UdpRequestSender {

    let (tx, rx) = sync_channel::<Packet>(256);
    let socket = udp_listener(listen_addr, tx.clone())
        .expect("Failed to start udp listener.");
    thread::spawn(move || {
        udp_packet_processor(socket, rx);
    });
    UdpRequestSender(tx)
}

pub fn query_udp_tracker(hash: [i8;20], tracker: SocketAddr) -> std::io::Result<()> {
    Ok(())
}
